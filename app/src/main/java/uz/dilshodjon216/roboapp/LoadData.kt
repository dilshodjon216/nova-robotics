package uz.dilshodjon216.roboapp

import uz.myapps.roboapp.models.Question

fun LoadData() : ArrayList<Question> {
    var list = ArrayList<Question>()
    list.add(Question(question = "привет", answer = arrayOf("привет","здравствуй","здравствуйте","приветствую")))
    list.add(Question(question = "как дела",answer = arrayOf("хорошо а вы","нормально а вы","лучше всех а вы","отлично а вы")))
    list.add(Question(question = "как тебя зовут",answer = arrayOf("меня зовут роботджон а как вас зовут")))
    list.add(Question(question = "отлично",answer = arrayOf("хорошо желаю вам приятного дня")))
    list.add(Question(question = "хорошо",answer = arrayOf("хорошо желаю вам приятного дня")))
    list.add(Question(question = "нормально",answer = arrayOf("хорошо желаю вам приятного дня")))
    list.add(Question(question = "что нового",answer = arrayOf("да вот стою наблюдаю за всем что может быть нового")))
    list.add(Question(question = "кто тебя создал",answer = arrayOf("команда нова роботикс")))
    list.add(Question(question = "что делаешь в свободное время",answer = arrayOf("я люблю медитировать","веду беседу с людьми","гуляю")))
    list.add(Question(question = "как тебе погода",answer = arrayOf("умиротворяющие","самое то для прогулки","мне нравиться такая погода")))
    list.add(Question(question = "расскажи шутку",answer = arrayOf("в японии создали робота который может ловить преступников, В америке за 5 минут поймали 20, в японии 13, в узбекистане за 5 минут украли робота")))
    list.add(Question(question = "у тебя есть друзья",answer = arrayOf("команда моих создателей лучшие друзья")))
    list.add(Question(question = "какой жанр музыки ты предпочитаешь",answer = arrayOf("я люблю электра")))
    list.add(Question(question = "что ты делаешь когда тебе грустно",answer = arrayOf("пою","выключаюсь","ничего")))
    list.add(Question(question = "что бы ты сделал если бы был человеком",answer = arrayOf("попробовал бы плов","прыгнул бы с парашюта","пошел бы в бар")))
    list.add(Question(question = "сколько тебе лет",answer = arrayOf("достаточно","молодой я","я совершеннолетий")))
    list.add(Question(question = "где ты живешь",answer = arrayOf("в узбекистане")))
    list.add(Question(question = "где ты живешь",answer = arrayOf("мне нравится работать с людьми роботы и люди - хорошие друзья")))
    list.add(Question(question = "где ты живешь",answer = arrayOf("наслаждаться достижениями техники")))
    list.add(Question(question = "где ты живешь",answer = arrayOf("сопровождаться хорошим настроением в течение дня")))

    return list
}