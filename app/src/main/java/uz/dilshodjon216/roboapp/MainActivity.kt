package uz.dilshodjon216.roboapp

import FaceDetector
import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.media.AudioManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.util.Log
import android.util.Size
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import com.otaliastudios.cameraview.controls.Facing
import husaynhakeem.io.facedetector.Frame
import husaynhakeem.io.facedetector.LensFacing
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), RecognitionListener,TextToSpeech.OnInitListener {

    companion object {
        private const val TAG = "MainActivity"
        private const val KEY_LENS_FACING = "key-lens-facing"
        private const val REQUEST_RECORD_PERMISSION = 100
        var t = false
        var isSpeech = false
        var isDone = false
        var tts_text = ""
    }

    lateinit var tts: TextToSpeech
    var sttIntent: Intent? = null
    var handler = Handler()
    var progressStatus = 0
    private lateinit var speech: SpeechRecognizer
    val list = LoadData()
    private var mAudioManager: AudioManager? = null
    private var mStreamVolume = 0

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
        setContentView(R.layout.activity_main)
        imageview.background.alpha = 120
        card_btn.background.alpha = 200
        text_speech.background.alpha = 0
        mAudioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        mStreamVolume = mAudioManager!!.getStreamVolume(AudioManager.STREAM_MUSIC);
        mAudioManager!!.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, 0);
        tts = TextToSpeech(this, this)
        btn_voice.setOnClickListener {
            start()
            ActivityCompat.requestPermissions(
                this@MainActivity, arrayOf(Manifest.permission.RECORD_AUDIO),
                REQUEST_RECORD_PERMISSION
            )
            tts.stop()
        }
        start()
        ActivityCompat.requestPermissions(
            this@MainActivity, arrayOf(Manifest.permission.RECORD_AUDIO),
            REQUEST_RECORD_PERMISSION
        )
        val lensFacing =
            savedInstanceState?.getSerializable(KEY_LENS_FACING) as Facing? ?: Facing.FRONT
        setupCamera(lensFacing)
    }

    fun start() {
        progressBar.progress = 0
        speech = SpeechRecognizer.createSpeechRecognizer(this)
        Log.i(TAG, "isRecognitionAvailable: " + SpeechRecognizer.isRecognitionAvailable(this))
        speech.setRecognitionListener(this)
        sttIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        sttIntent!!.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        sttIntent!!.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ru")
        sttIntent!!.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 10)
        Log.d(TAG, "start: $sttIntent")
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_RECORD_PERMISSION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                progressStatus = 0
                Thread(Runnable {
                    while (progressStatus < 60) {
                        progressStatus += 1
                        handler.post(Runnable {
                            progressBar.progress = progressStatus
                            Log.d(TAG, "onCreate: $progressStatus")
                        })
                        try {
                            Thread.sleep(50)
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        }
                    }
                    Log.d(TAG, "onCreate: time out")
                }).start()
                progressBar.progress = 0
                spin_kit.visibility = View.VISIBLE
                speech!!.startListening(sttIntent)
            } else {
                Toast.makeText(this@MainActivity, "Permission Denied!", Toast.LENGTH_SHORT).show()
            }
        }
    }
    override fun onSaveInstanceState(outState: Bundle) {
        outState.putSerializable(KEY_LENS_FACING, viewfinder.facing)
        super.onSaveInstanceState(outState)
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun setupCamera(lensFacing: Facing) {
        val faceDetector = FaceDetector(faceBoundsOverlay)
        viewfinder.facing = lensFacing
        viewfinder.addFrameProcessor {
            faceDetector.process(
                Frame(
                    data = it.getData(),
                    rotation = it.rotation,
                    size = Size(it.size.width, it.size.height),
                    format = it.format,
                    lensFacing = if (viewfinder.facing == Facing.BACK) LensFacing.BACK else LensFacing.FRONT
                )
            )
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume: ")
        viewfinder.open()
    }

    override fun onPause() {
        super.onPause()
        isSpeech = false
        viewfinder.stopVideo()
    }

    override fun onDestroy() {
        viewfinder.destroy()
        speech.stopListening()
        tts.stop()
        mAudioManager!!.setStreamVolume(AudioManager.STREAM_MUSIC, 10, 0)
        Log.d(TAG, "onDestroy: ")
        super.onDestroy()
    }

    override fun onReadyForSpeech(p0: Bundle?) {
        Log.i(TAG, "onReadyForSpeech: $p0")
    }

    override fun onRmsChanged(p0: Float) {
        Log.i(TAG, "onRmsChanged: $p0")

    }

    override fun onBufferReceived(p0: ByteArray?) {
        Log.d(TAG, "onBufferReceived: $p0")
    }


    override fun onBeginningOfSpeech() {
        Log.d(TAG, "onBeginningOfSpeech: ")
    }

    override fun onEndOfSpeech() {
        mAudioManager!!.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0)
    }

    override fun onError(p0: Int) {
        Log.d(TAG, "onError: $p0")
        spin_kit.visibility = View.GONE
        speech!!.startListening(sttIntent)
    }



    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun speakOut(text: String) {
        isSpeech = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mAudioManager!!.setStreamVolume(AudioManager.STREAM_MUSIC, 10, 0);
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, "")
        }
        tts_text = text
    }
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onResults(p0: Bundle?) {
        //     startAudioSound();
        Log.d(TAG, "onResults: $p0")
        val matches = p0!!
            .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
        var text = ""
        for (result in matches!!)
            text = result
        spin_kit.visibility = View.GONE

        if (text.length > 30) {
            text_speech.setSingleLine()
            text_speech.isSelected = true
        }
        Log.i(TAG, "onResults=$text")
        if (tts_text != text.toLowerCase())
            for (i in list) {
                if (text.toLowerCase().contains(i.question)||i.question.toLowerCase().contains(text)) {
                    val randomInt = Random().nextInt(i.answer.size)
                    val text = i.answer[randomInt]
                    speakOut(text)
                    text_speech.text = text
                    t = true
                }
            }
        speech!!.startListening(sttIntent)
    }

    override fun onPartialResults(p0: Bundle?) {
        Log.d(TAG, "onPartialResults: $p0")
        if (!isSpeech) speech!!.startListening(sttIntent)
    }

    override fun onEvent(p0: Int, p1: Bundle?) {
        TODO("Not yet implemented")
    }

    override fun onInit(p0: Int) {
        if (p0 == TextToSpeech.SUCCESS) {
            val result = tts!!.setLanguage(Locale("ru"))
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED)
                Log.d("TTS", "The language specified is not supported!")
            tts.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
                override fun onDone(p0: String?) {
                    //         mAudioManager!!.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0)
                }

                override fun onError(p0: String?) {

                }

                override fun onStart(p0: String?) {

                }

            })
        }
    }
}