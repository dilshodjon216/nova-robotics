package uz.dilshodjon216.roboapp

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class SplashActivity : AppCompatActivity() {
    private lateinit var sharedPreferences: SharedPreferences
    private var UNICAL_KEY = "KEY"
    private var setup:String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        sharedPreferences = this.getSharedPreferences(UNICAL_KEY,MODE_PRIVATE)
        setup= sharedPreferences.getString("app_setup","").toString()

        if(setup == "true"){

           var handler = Handler()
            handler.postDelayed({
                val intent = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(intent)
                finish()
            }, 3000)
        }else
        {
            var handler = Handler()
            handler.postDelayed({
                val intent = Intent(this@SplashActivity, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }, 3000)
        }


    }
}