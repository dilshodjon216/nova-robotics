package uz.myapps.roboapp.models

//@Entity(tableName = "Dialog")
data class Question(
//    @PrimaryKey(autoGenerate = true)
//    var id: Int = 0,
    var question: String,
    var answer: Array<String>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Question

        if (question != other.question) return false
        if (!answer.contentEquals(other.answer)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = question.hashCode()
        result = 31 * result + answer.contentHashCode()
        return result
    }
}





